package org.openchargemap.dummyapp.types;

import java.util.Objects;

/**
 * This class is internal Connector POJO
 */
public class Connection {
    private Integer connectionId;
    private Integer amps;
    private Integer voltage;
    private Double powerKw;
    private Integer quantity;

    public Integer getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Integer connectionId) {
        this.connectionId = connectionId;
    }

    public Integer getAmps() {
        return amps;
    }

    public void setAmps(Integer amps) {
        this.amps = amps;
    }

    public Integer getVoltage() {
        return voltage;
    }

    public void setVoltage(Integer voltage) {
        this.voltage = voltage;
    }

    public Double getPowerKw() {
        return powerKw;
    }

    public void setPowerKw(Double powerKw) {
        this.powerKw = powerKw;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Connection)) return false;
        Connection that = (Connection) o;
        return Objects.equals(connectionId, that.connectionId) &&
                Objects.equals(amps, that.amps) &&
                Objects.equals(voltage, that.voltage) &&
                Objects.equals(powerKw, that.powerKw) &&
                Objects.equals(quantity, that.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(connectionId, amps, voltage, powerKw, quantity);
    }

    @Override
    public String toString() {
        return "Connection{" +
                "connectionId=" + connectionId +
                ", amps=" + amps +
                ", voltage=" + voltage +
                ", powerKw=" + powerKw +
                ", quantity=" + quantity +
                '}';
    }
}
