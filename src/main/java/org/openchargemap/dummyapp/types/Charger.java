package org.openchargemap.dummyapp.types;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class is internal Charger POJO
 */
public class Charger {
    private Integer chargerId;
    private String uuid;
    private String town;
    private String postcode;
    private String latitude;
    private String longitude;
    private List<Connection> connections = new ArrayList<>();

    public Integer getChargerId() {
        return chargerId;
    }

    public void setChargerId(Integer chargerId) {
        this.chargerId = chargerId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public List<Connection> getConnections() {
        return connections;
    }

    public void setConnections(List<Connection> connections) {
        this.connections = connections;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Charger)) return false;
        Charger charger = (Charger) o;
        return Objects.equals(chargerId, charger.chargerId) &&
                Objects.equals(uuid, charger.uuid) &&
                Objects.equals(town, charger.town) &&
                Objects.equals(postcode, charger.postcode) &&
                Objects.equals(latitude, charger.latitude) &&
                Objects.equals(longitude, charger.longitude) &&
                Objects.equals(connections, charger.connections);
    }

    @Override
    public int hashCode() {
        return Objects.hash(chargerId, uuid, town, postcode, latitude, longitude, connections);
    }

    @Override
    public String toString() {
        return "Charger{" +
                "chargerId=" + chargerId +
                ", uuid='" + uuid + '\'' +
                ", town='" + town + '\'' +
                ", postcode='" + postcode + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", connections=" + connections +
                '}';
    }
}
