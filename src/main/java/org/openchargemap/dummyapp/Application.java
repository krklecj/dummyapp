package org.openchargemap.dummyapp;

import org.openchargemap.dummyapp.services.OpenChargeMapFetchAndPersist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "org.openchargemap.dummyapp")
public class Application implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    @Autowired
    private OpenChargeMapFetchAndPersist openChargeMapFetchAndPersist;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        openChargeMapFetchAndPersist.fetchAndPersist();
    }
}
