package org.openchargemap.dummyapp.db;

import org.openchargemap.dummyapp.db.types.ChargerDbEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ChargerRepository extends JpaRepository<ChargerDbEntity, Integer>, JpaSpecificationExecutor {
    @Transactional
    int deleteByChargerId(Integer chargerId);

    @Query(value = "select c from ChargerDbEntity c where c.id in (select c.charger from ConnectionDbEntity c where c.powerKw > (select avg(c.powerKw) from ConnectionDbEntity c))")
    List<ChargerDbEntity> findAllChargersWithPowerGreaterThanAverage();
}
