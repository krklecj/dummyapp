package org.openchargemap.dummyapp.db;

import org.openchargemap.dummyapp.db.types.ConnectionDbEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnectionRepository extends JpaRepository<ConnectionDbEntity, Integer> {
}
