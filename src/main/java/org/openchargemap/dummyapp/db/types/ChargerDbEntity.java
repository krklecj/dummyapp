package org.openchargemap.dummyapp.db.types;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "chargers")
public class ChargerDbEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(unique = true)
    private Integer chargerId;
    private String uuid;
    private String town;
    private String postcode;
    private String latitude;
    private String longitude;
    @OneToMany(mappedBy = "charger", cascade = CascadeType.ALL)
    private List<ConnectionDbEntity> connections;

    public ChargerDbEntity() {
    }

    public ChargerDbEntity(org.openchargemap.dummyapp.types.Charger charger) {
        this.chargerId = charger.getChargerId();
        this.uuid = charger.getUuid();
        this.town = charger.getTown();
        this.postcode = charger.getPostcode();
        this.latitude = charger.getLatitude();
        this.longitude = charger.getLongitude();
        this.connections = new ArrayList<>();
        for (org.openchargemap.dummyapp.types.Connection connection : charger.getConnections()) {
            this.connections.add(new ConnectionDbEntity(connection));
        }
        this.connections.forEach(x -> x.setCharger(this));
    }

    public Integer getId() {
        return id;
    }

    public Integer getChargerId() {
        return chargerId;
    }

    public String getUuid() {
        return uuid;
    }

    public String getTown() {
        return town;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public List<ConnectionDbEntity> getConnections() {
        return connections;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChargerDbEntity)) return false;
        ChargerDbEntity that = (ChargerDbEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(chargerId, that.chargerId) &&
                Objects.equals(uuid, that.uuid) &&
                Objects.equals(town, that.town) &&
                Objects.equals(postcode, that.postcode) &&
                Objects.equals(latitude, that.latitude) &&
                Objects.equals(longitude, that.longitude) &&
                Objects.equals(connections, that.connections);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, chargerId, uuid, town, postcode, latitude, longitude, connections);
    }

    @Override
    public String toString() {
        return "ChargerDbEntity{" +
                "id=" + id +
                ", chargerId=" + chargerId +
                ", uuid='" + uuid + '\'' +
                ", town='" + town + '\'' +
                ", postcode='" + postcode + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", connections=" + connections +
                '}';
    }
}
