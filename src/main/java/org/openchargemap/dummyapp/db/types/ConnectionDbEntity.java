package org.openchargemap.dummyapp.db.types;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "connections")
public class ConnectionDbEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(unique = true)
    private Integer connectionId;
    private Integer amps;
    private Integer voltage;
    private Double powerKw;
    private Integer quantity;
    @ManyToOne
    @JoinColumn
    private ChargerDbEntity charger;

    public ConnectionDbEntity() {
    }

    public ConnectionDbEntity(org.openchargemap.dummyapp.types.Connection connection) {
        this.connectionId = connection.getConnectionId();
        this.amps = connection.getAmps();
        this.voltage = connection.getVoltage();
        this.powerKw = connection.getPowerKw();
        this.quantity = connection.getQuantity();
    }

    public void setCharger(ChargerDbEntity charger) {
        this.charger = charger;
    }

    public Integer getId() {
        return id;
    }

    public Integer getConnectionId() {
        return connectionId;
    }

    public Integer getAmps() {
        return amps;
    }

    public Integer getVoltage() {
        return voltage;
    }

    public Double getPowerKw() {
        return powerKw;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public ChargerDbEntity getCharger() {
        return charger;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConnectionDbEntity)) return false;
        ConnectionDbEntity that = (ConnectionDbEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(connectionId, that.connectionId) &&
                Objects.equals(amps, that.amps) &&
                Objects.equals(voltage, that.voltage) &&
                Objects.equals(powerKw, that.powerKw) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(charger, that.charger);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, connectionId, amps, voltage, powerKw, quantity, charger);
    }

    @Override
    public String toString() {
        return "ConnectionDbEntity{" +
                "id=" + id +
                ", connectionId=" + connectionId +
                ", amps=" + amps +
                ", voltage=" + voltage +
                ", powerKw=" + powerKw +
                ", quantity=" + quantity +
                ", charger=" + charger +
                '}';
    }
}
