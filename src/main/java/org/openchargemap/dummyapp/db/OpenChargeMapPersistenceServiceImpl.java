package org.openchargemap.dummyapp.db;

import org.openchargemap.dummyapp.db.types.ChargerDbEntity;
import org.openchargemap.dummyapp.db.types.ConnectionDbEntity;
import org.openchargemap.dummyapp.services.OpenChargeMapPersistenceService;
import org.openchargemap.dummyapp.types.Charger;
import org.openchargemap.dummyapp.types.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * This service is responsible for all and only persistence-related functionality of an app
 */
@Component
public class OpenChargeMapPersistenceServiceImpl implements OpenChargeMapPersistenceService {
    private static final Logger logger = LoggerFactory.getLogger(OpenChargeMapPersistenceServiceImpl.class);

    @Autowired
    private ChargerRepository chargerRepository;

    @Override
    public List<Charger> findAllChargers() {
        logger.debug("Getting all chargers from DB");
        List<ChargerDbEntity> chargerDbEntities = chargerRepository.findAll();
        logger.debug("Fetched all chargers from DB {}", chargerDbEntities);
        return buildChargersFromChargerDbEntities(chargerDbEntities);
    }

    @Override
    public List<Charger> findAllChargers(String postcode) {
        logger.debug("Getting all chargers from DB for postcode {}", postcode);
        List<ChargerDbEntity> chargerDbEntities = chargerRepository.findAll(ChargerSpecifications.byPostcode(postcode));
        logger.debug("Fetched all chargers from DB with postcode {}, {}", postcode, chargerDbEntities);
        return buildChargersFromChargerDbEntities(chargerDbEntities);
    }

    @Override
    public List<Charger> findAllChargersWithPowerGreaterThanAverage() {
        logger.debug("Getting all chargers from DB with power greater than average");
        List<ChargerDbEntity> chargerDbEntities = chargerRepository.findAllChargersWithPowerGreaterThanAverage();
        logger.debug("Fetched all chargers from DB with power greater than average {}", chargerDbEntities);
        return buildChargersFromChargerDbEntities(chargerDbEntities);
    }

    @Override
    public Integer deleteCharger(Integer chargerId) {
        logger.debug("Deleting charger from DB with charger id {}", chargerId);
        return chargerRepository.deleteByChargerId(chargerId);
    }

    @Override
    public void saveCharger(Charger charger) throws OpenChargeMapPersistenceServiceException {
        logger.debug("Saving charger to DB {}", charger);
        try {
            chargerRepository.save(new ChargerDbEntity(charger));
        } catch (DataIntegrityViolationException dataIntegrityViolationException) {
            logger.debug("saveCharger failed for Charger {}", charger, dataIntegrityViolationException);
            throw new OpenChargeMapPersistenceServiceException(new StringBuilder("saveCharger failed for Charger ").append(charger.toString()).toString(), dataIntegrityViolationException);
        }
    }

    private List<Charger> buildChargersFromChargerDbEntities(List<ChargerDbEntity> chargerDbEntities) {
        List<Charger> chargers = new ArrayList<>();
        if (chargerDbEntities != null) {
            Stream<ChargerDbEntity> chargerDbEntityStream = chargerDbEntities.stream();
            chargerDbEntityStream.filter(Objects::nonNull).forEach(chargerDbEntity -> chargers.add(buildChargerFromChargerDbEntity(chargerDbEntity)));
        }
        return chargers;
    }

    private Charger buildChargerFromChargerDbEntity(ChargerDbEntity chargerDbEntity) {
        Charger charger = new Charger();
        charger.setChargerId(chargerDbEntity.getChargerId());
        charger.setUuid(chargerDbEntity.getUuid());
        charger.setTown(chargerDbEntity.getTown());
        charger.setPostcode(chargerDbEntity.getPostcode());
        charger.setLongitude(chargerDbEntity.getLongitude());
        charger.setLatitude(chargerDbEntity.getLatitude());
        charger.setConnections(buildConnectionsFromConnectionDbEntities(chargerDbEntity.getConnections()));
        return charger;
    }

    private List<Connection> buildConnectionsFromConnectionDbEntities(List<ConnectionDbEntity> connectionDbEntities) {
        List<Connection> connections = new ArrayList<>();
        if (connectionDbEntities != null) {
            Stream<ConnectionDbEntity> connectionDbEntityStream = connectionDbEntities.stream();
            connectionDbEntityStream.filter(Objects::nonNull).forEach(connectionDbEntity -> connections.add(buildConnectionFromConnectionDbEntity(connectionDbEntity)));
        }
        return connections;
    }

    private Connection buildConnectionFromConnectionDbEntity(ConnectionDbEntity connectionDbEntity) {
        Connection connection = new Connection();
        connection.setConnectionId(connectionDbEntity.getConnectionId());
        connection.setVoltage(connectionDbEntity.getVoltage());
        connection.setQuantity(connectionDbEntity.getQuantity());
        connection.setPowerKw(connectionDbEntity.getPowerKw());
        connection.setAmps(connectionDbEntity.getAmps());
        return connection;
    }
}
