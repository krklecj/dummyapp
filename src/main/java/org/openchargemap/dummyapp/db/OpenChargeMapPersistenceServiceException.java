package org.openchargemap.dummyapp.db;

public class OpenChargeMapPersistenceServiceException extends Exception {
    public OpenChargeMapPersistenceServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
