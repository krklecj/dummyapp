package org.openchargemap.dummyapp.db;

import org.springframework.data.jpa.domain.Specification;

public interface ChargerSpecifications {
    static <Charger> Specification<Charger> byPostcode(String postcode) {
        return (Specification<Charger>) (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("postcode"), postcode);
    }
}
