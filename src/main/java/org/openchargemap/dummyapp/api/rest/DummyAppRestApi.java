package org.openchargemap.dummyapp.api.rest;

import org.openchargemap.dummyapp.services.OpenChargeMapPersistenceService;
import org.openchargemap.dummyapp.types.Charger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/dummyapp")
public class DummyAppRestApi {
    private static final Logger logger = LoggerFactory.getLogger(DummyAppRestApi.class);

    @Autowired
    private OpenChargeMapPersistenceService openChargeMapPersistenceService;

    @GetMapping("/chargers")
    public ResponseEntity<List<Charger>> getChargers(@RequestParam(defaultValue = "false") Boolean powerKw) {
        logger.debug("GET chargers with powerKw {}", powerKw);
        List<Charger> chargers = null;
        if (powerKw) {
            chargers = openChargeMapPersistenceService.findAllChargersWithPowerGreaterThanAverage();
        } else {
            chargers = openChargeMapPersistenceService.findAllChargers();
        }
        logger.debug("GET chargers with powerKw {} {}", powerKw, chargers);
        return ResponseEntity.ok(chargers);
    }

    @GetMapping("/chargers/{postcode}")
    public ResponseEntity<List<Charger>> getChargersByPostcode(@PathVariable(value = "postcode") String postcode) {
        logger.debug("GET chargers for postcode {}", postcode);
        List<Charger> chargers = openChargeMapPersistenceService.findAllChargers(postcode);
        logger.debug("GET chargers for postcode {} {}", postcode, chargers);
        return ResponseEntity.ok(chargers);
    }

    @DeleteMapping("/charger/{chargerId}")
    public ResponseEntity deleteChargerByChargerId(@PathVariable(value = "chargerId") Integer chargerId) {
        logger.debug("DELETE charger for charger id {}", chargerId);
        ResponseEntity responseEntity;
        Integer deleted = openChargeMapPersistenceService.deleteCharger(chargerId);
        if (deleted != 0) {
            responseEntity = ResponseEntity.ok().build();
        } else {
            responseEntity = ResponseEntity.notFound().build();
        }
        return responseEntity;
    }
}
