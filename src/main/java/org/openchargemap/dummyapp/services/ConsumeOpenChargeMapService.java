package org.openchargemap.dummyapp.services;

import org.openchargemap.dummyapp.types.Charger;

import java.util.List;

/**
 * This service is responsible for consuming openchargemap API
 */
public interface ConsumeOpenChargeMapService {
    List<Charger> getChargersFromService();
}
