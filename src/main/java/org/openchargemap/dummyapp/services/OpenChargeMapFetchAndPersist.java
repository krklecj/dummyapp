package org.openchargemap.dummyapp.services;

import org.openchargemap.dummyapp.db.OpenChargeMapPersistenceServiceException;
import org.openchargemap.dummyapp.types.Charger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * This service contains business logic for fetching data from openchargemap API and persisting data
 */
@Component
public class OpenChargeMapFetchAndPersist {
    private static final Logger logger = LoggerFactory.getLogger(OpenChargeMapFetchAndPersist.class);

    @Autowired
    private ConsumeOpenChargeMapService consumeOpenChargeMapService;

    @Autowired
    private OpenChargeMapPersistenceService openChargeMapPersistenceService;

    public void fetchAndPersist() {
        List<Charger> chargers = consumeOpenChargeMapService.getChargersFromService();
        logger.debug("Created charger objects {}", chargers);
        for (Charger charger : chargers) {
            try {
                openChargeMapPersistenceService.saveCharger(charger);
            } catch (OpenChargeMapPersistenceServiceException openChargeMapPersistenceServiceException) {
                logger.debug("saveCharger failed for Charger {}", charger, openChargeMapPersistenceServiceException);
            }
        }
    }
}
