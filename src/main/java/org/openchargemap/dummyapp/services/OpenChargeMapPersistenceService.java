package org.openchargemap.dummyapp.services;

import org.openchargemap.dummyapp.db.OpenChargeMapPersistenceServiceException;
import org.openchargemap.dummyapp.types.Charger;

import java.util.List;

/**
 * This service provides functionality for persisting data
 */
public interface OpenChargeMapPersistenceService {
    List<Charger> findAllChargers();

    List<Charger> findAllChargers(String postcode);

    List<Charger> findAllChargersWithPowerGreaterThanAverage();

    Integer deleteCharger(Integer chargerId);

    void saveCharger(Charger charger) throws OpenChargeMapPersistenceServiceException;
}
