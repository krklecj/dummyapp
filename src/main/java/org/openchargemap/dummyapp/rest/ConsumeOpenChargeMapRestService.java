package org.openchargemap.dummyapp.rest;

import org.openchargemap.dummyapp.rest.types.ChargerRestEntity;
import org.openchargemap.dummyapp.rest.types.ConnectionRestEntity;
import org.openchargemap.dummyapp.services.ConsumeOpenChargeMapService;
import org.openchargemap.dummyapp.types.Charger;
import org.openchargemap.dummyapp.types.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * This service is responsible consuming openchargemap REST service only
 */
@Component
public class ConsumeOpenChargeMapRestService implements ConsumeOpenChargeMapService {
    private static final Logger logger = LoggerFactory.getLogger(ConsumeOpenChargeMapRestService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${endpoint}")
    private String restEndpoint;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Override
    public List<Charger> getChargersFromService() {
        logger.debug("Fetching chargers from REST web service {}", restEndpoint);
        ChargerRestEntity[] restChargers = restTemplate.getForObject(restEndpoint, ChargerRestEntity[].class);
        logger.debug("Fetched chargers from REST web service: {}", restChargers);
        return buildChargersFromRestTypes(restChargers);
    }

    private List<Charger> buildChargersFromRestTypes(ChargerRestEntity[] chargerRestEntities) {
        List<Charger> chargers = new ArrayList<>();
        if (chargerRestEntities != null) {
            Stream<ChargerRestEntity> chargerRestEntityStream = Stream.of(chargerRestEntities);
            chargerRestEntityStream.filter(Objects::nonNull).forEach(chargerRestEntity -> chargers.add(buildChargerFromChargerRestEntity(chargerRestEntity)));
        }
        return chargers;
    }

    private Charger buildChargerFromChargerRestEntity(ChargerRestEntity chargerRestEntity) {
        Charger charger = new Charger();
        charger.setChargerId(chargerRestEntity.getId());
        charger.setUuid(chargerRestEntity.getUuid());
        if (chargerRestEntity.getAddressInfo() != null) {
            charger.setLatitude(chargerRestEntity.getAddressInfo().getLatitude());
            charger.setLongitude(chargerRestEntity.getAddressInfo().getLongitude());
            charger.setPostcode(chargerRestEntity.getAddressInfo().getPostcode());
            charger.setTown(chargerRestEntity.getAddressInfo().getTown());
        }
        charger.setConnections(buildConnectionsFromConnectionRestEntities(chargerRestEntity.getConnections()));
        return charger;
    }

    private List<Connection> buildConnectionsFromConnectionRestEntities(List<ConnectionRestEntity> connectionRestEntities) {
        List<Connection> connections = new ArrayList<>();
        if (connectionRestEntities != null) {
            Stream<ConnectionRestEntity> connectionRestEntityStream = connectionRestEntities.stream();
            connectionRestEntityStream.filter(Objects::nonNull).forEach(connectionRestEntity -> connections.add(buildConnectionFromConnectionRestEntity(connectionRestEntity)));
        }
        return connections;
    }

    private Connection buildConnectionFromConnectionRestEntity(ConnectionRestEntity connectionRestEntity) {
        Connection connection = new Connection();
        connection.setConnectionId(connectionRestEntity.getId());
        connection.setAmps(connectionRestEntity.getAmps());
        connection.setPowerKw(connectionRestEntity.getPowerKw());
        connection.setQuantity(connectionRestEntity.getQuantity());
        connection.setVoltage(connectionRestEntity.getVoltage());
        return connection;
    }
}
