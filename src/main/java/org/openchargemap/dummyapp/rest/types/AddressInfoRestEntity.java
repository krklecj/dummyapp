package org.openchargemap.dummyapp.rest.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddressInfoRestEntity {
    @JsonProperty("Town")
    private String town;
    @JsonProperty("Postcode")
    private String postcode;
    @JsonProperty("Latitude")
    private String latitude;
    @JsonProperty("Longitude")
    private String longitude;

    public AddressInfoRestEntity() {
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AddressInfoRestEntity)) return false;
        AddressInfoRestEntity that = (AddressInfoRestEntity) o;
        return Objects.equals(town, that.town) &&
                Objects.equals(postcode, that.postcode) &&
                Objects.equals(latitude, that.latitude) &&
                Objects.equals(longitude, that.longitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(town, postcode, latitude, longitude);
    }

    @Override
    public String toString() {
        return "AddressInfo{" +
                "town='" + town + '\'' +
                ", postcode='" + postcode + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
