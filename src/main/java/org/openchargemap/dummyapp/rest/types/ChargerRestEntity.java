package org.openchargemap.dummyapp.rest.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ChargerRestEntity {
    @JsonProperty("ID")
    private Integer id;
    @JsonProperty("UUID")
    private String uuid;
    @JsonProperty("AddressInfo")
    private AddressInfoRestEntity addressInfo;
    @JsonProperty("Connections")
    private List<ConnectionRestEntity> connections;

    public ChargerRestEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public AddressInfoRestEntity getAddressInfo() {
        return addressInfo;
    }

    public void setAddressInfo(AddressInfoRestEntity addressInfo) {
        this.addressInfo = addressInfo;
    }

    public List<ConnectionRestEntity> getConnections() {
        return connections;
    }

    public void setConnections(List<ConnectionRestEntity> connections) {
        this.connections = connections;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChargerRestEntity)) return false;
        ChargerRestEntity chargers = (ChargerRestEntity) o;
        return Objects.equals(id, chargers.id) &&
                Objects.equals(uuid, chargers.uuid) &&
                Objects.equals(addressInfo, chargers.addressInfo) &&
                Objects.equals(connections, chargers.connections);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, uuid, addressInfo, connections);
    }

    @Override
    public String toString() {
        return "Charger{" +
                "id=" + id +
                ", uuid='" + uuid + '\'' +
                ", addressInfo=" + addressInfo +
                ", connections=" + connections +
                '}';
    }
}
