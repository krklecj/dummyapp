package org.openchargemap.dummyapp.rest.types;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ConnectionRestEntity {
    @JsonProperty("ID")
    private Integer id;
    @JsonProperty("Amps")
    private Integer amps;
    @JsonProperty("Voltage")
    private Integer voltage;
    @JsonProperty("PowerKW")
    private Double powerKw;
    @JsonProperty("Quantity")
    private Integer quantity;

    public ConnectionRestEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmps() {
        return amps;
    }

    public void setAmps(Integer amps) {
        this.amps = amps;
    }

    public Integer getVoltage() {
        return voltage;
    }

    public void setVoltage(Integer voltage) {
        this.voltage = voltage;
    }

    public Double getPowerKw() {
        return powerKw;
    }

    public void setPowerKw(Double powerKw) {
        this.powerKw = powerKw;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConnectionRestEntity)) return false;
        ConnectionRestEntity that = (ConnectionRestEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(amps, that.amps) &&
                Objects.equals(voltage, that.voltage) &&
                Objects.equals(powerKw, that.powerKw) &&
                Objects.equals(quantity, that.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, amps, voltage, powerKw, quantity);
    }

    @Override
    public String toString() {
        return "Connection{" +
                "id=" + id +
                ", amps=" + amps +
                ", voltage=" + voltage +
                ", powerKw=" + powerKw +
                ", quantity=" + quantity +
                '}';
    }
}
